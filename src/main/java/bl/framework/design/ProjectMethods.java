package bl.framework.design;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import bl.framework.api.SeleniumBase;
import util.ReadExcel;

public class ProjectMethods extends SeleniumBase{
	/*ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;*/
	
	public String testcaseName, testDec, author, category,Sheetname;
	
@BeforeMethod
public void login()
{startApp("chrome", "http://leaftaps.com/opentaps");
WebElement usernameip = locateElement("id","username");		
clearAndType(usernameip,"DemoSalesManager");
WebElement passwordip = locateElement("id","password");
clearAndType(passwordip, "crmsfa");
WebElement eleLogin = locateElement("class", "decorativeSubmit");
click(eleLogin); 
}


@BeforeSuite
public void beforeSuite() {
	startreport();
}
@AfterSuite
public void afterSuite() {
	endReport();
}
@BeforeClass
public void beforeClass() {
	initializeTest(testcaseName, testDec, author, category);
}
//@BeforeMethod

@AfterMethod
public void closeApp() {
	close();
}
@DataProvider(name="fetchData")
public Object[][] getdata()
{
	return ReadExcel.readData(Sheetname);
}

}
	












	
	
/*
	@BeforeSuiteefor
	public void startreport()
	{
		
		//create Template for the reports in the mentioned path 
		html= new ExtentHtmlReporter("./report/extentRports.html");
		//object created for test logs
		extent= new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
	}
	
	@BeforeTest
	public void initializeTest(String testcaseName,String testDec, String author, String category)
	{
		test=extent.createTest(testcaseName,testDec);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
		public void logstep(String des,String status)
		{
			if (status.equalsIgnoreCase("pass")) {
				test.pass(des);
			} else if (status.equalsIgnoreCase("fail")) {
				test.fail(des);
				throw new RuntimeException();
			} else if (status.equalsIgnoreCase("warning")) {
				test.warning(des);
			}

		}
		@AfterSuite
		public void endReport()
		{
	extent.flush();
	}

		*/
			

