package bl.framework.testcases;




import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods  
{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testDec = "Edit  Lead in leaftaps";
		author = "Gayatri1";
		category = "Smoke1";
		Sheetname="TC001";
	}
	
	@Test(dataProvider="fetchData")
	public void createLead(String cname,String fname,String lname)
	{
		/*startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement usernameip = locateElement("id","username");		
		clearAndType(usernameip,"DemoSalesManager");
		WebElement passwordip = locateElement("id","password");
		clearAndType(passwordip, "crmsfa");
		WebElement login = locateElement("class","decorativeSubmit");
		click(login);
		*/
		//login();
		WebElement crmsfa = locateElement("link", "CRM/SFA");
		click(crmsfa);
		WebElement createlead = locateElement("link","Create Lead");
		click(createlead);
		WebElement compname = locateElement("id","createLeadForm_companyName");
		clearAndType(compname,cname);
		WebElement firstname = locateElement("id","createLeadForm_firstName");
		clearAndType(firstname,fname);
		WebElement lastname = locateElement("id","createLeadForm_lastName");
		clearAndType(lastname,lname);
		WebElement Title = locateElement("id","createLeadForm_generalProfTitle");
		clearAndType(Title,"God Bless You");
		
		
		WebElement leadcreate = locateElement("class","smallSubmit");
		click(leadcreate);
		
		
		
		
		
		
		
	}
	/*@DataProvider(name="getdata")
	public String[][] fetchdata()
	{
		String[][] data= new String[2][3];
		data[0][0]="TestLeaf";
		data[0][1]="Balaji";
		data[0][2]="C";
		
	
		data[1][0]="TestLeaf";
		data[1][1]="sarath";
		data[1][2]="T";
		return data;
	}
	*/
	
	

}





















/*
@Test(dependsOnMethods={"bl.framework.testcases.Tc004_EditLead.editLead"})
public void createLead()
{
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement usernameip = locateElement("id","username");		
	clearAndType(usernameip,"DemoSalesManager");
	WebElement passwordip = locateElement("id","password");
	clearAndType(passwordip, "crmsfa");
	WebElement login = locateElement("class","decorativeSubmit");
	click(login);
	
	//login();
	WebElement crmsfa = locateElement("linktext", "CRM/SFA");
	click(crmsfa);
	WebElement createlead = locateElement("linktext","Create Lead");
	click(createlead);
	WebElement compname = locateElement("id","createLeadForm_companyName");
	clearAndType(compname,"Dxc Technology");
	WebElement firstname = locateElement("id","createLeadForm_firstName");
	clearAndType(firstname,"Sudarvizhi");
	WebElement lastname = locateElement("id","createLeadForm_lastName");
	clearAndType(lastname,"SP");
	WebElement Title = locateElement("id","createLeadForm_generalProfTitle");
	clearAndType(Title,"God Bless You");
	
	
	WebElement leadcreate = locateElement("class","smallSubmit");
	click(leadcreate);
	*/
	
	
	
	

