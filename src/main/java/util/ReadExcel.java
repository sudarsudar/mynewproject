package util;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
static Object[][] data;
	public static Object[][] readData(String datasheet)  {
		// TODO Auto-generated method stub
//Get the path for Workbook
		XSSFWorkbook wb;
		try {
			wb = new XSSFWorkbook("./Data/"+datasheet+".xlsx");
		
		//Enter sheetname
		XSSFSheet sheet = wb.getSheet("Sheet1");
		//get the rowcount
		int lastRowNum = sheet.getLastRowNum();		
		//get columncount
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		System.out.println(lastRowNum);
		System.out.println(lastCellNum);
		data = new Object[lastRowNum][lastCellNum];
		for(int i=1;i<=lastRowNum;i++)
		{
			XSSFRow row = sheet.getRow(i);
			for(int j=0;j<lastCellNum;j++)
			{
			XSSFCell cell = row.getCell(j);
			String stringCellValue = cell.getStringCellValue();
			System.out.println(stringCellValue);
			}
		}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return  data;
	}

}
