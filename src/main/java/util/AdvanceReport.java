package util;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class AdvanceReport 
{
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;

	//public String testcaseName, testDec, author, category;
	//	@Test
	public void startreport()
	{

		//create Template for the reports in the mentioned path 
		html= new ExtentHtmlReporter("./report/extentRports.html");
		//object created for test logs
		extent= new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
	}
	public void initializeTest(String testcaseName,String testDec, String author, String category)
	{
		test=extent.createTest(testcaseName,testDec);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	public void logStep(String des,String status)
	{
		if (status.equalsIgnoreCase("pass")) {
			test.pass(des);
		} else if (status.equalsIgnoreCase("fail")) {
			test.fail(des);
			throw new RuntimeException();
		} else if (status.equalsIgnoreCase("warning")) {
			test.warning(des);
		}

	}
	public void endReport()
	{
		extent.flush();
	}





}


