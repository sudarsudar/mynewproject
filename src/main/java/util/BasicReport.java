package util;



import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport 
{
ExtentHtmlReporter html;
ExtentReports extent;
ExtentTest test;
@Test
public void runReport()
{
	
	//create Template for the reports in the mentioned path 
	html= new ExtentHtmlReporter("./report/extentRports.html");
	//object created for test logs
	extent= new ExtentReports();
	html.setAppendExisting(true);
	extent.attachReporter(html);
	test=extent.createTest("Tc001_Login", "Login to LeafTaps");
	test.assignAuthor("Sudar");
	test.assignCategory("smoke");
	
	try {
		test.fail("user name Demosales MAnager successfully Entered",
		MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
extent.flush();


}

	
	
	
	
	
	
	
	
	
	
	
}
